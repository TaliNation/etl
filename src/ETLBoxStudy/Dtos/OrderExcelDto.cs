﻿using ETLBox.DataFlow;

namespace ETLBoxStudy.Dtos
{
    public class OrderExcelDto
    {
        [DistinctColumn]
        [ExcelColumn(0)]
        public string Code { get; set; }

        [ExcelColumn(2)]
        public string SalesCode { get; set; }

        [ExcelColumn(1)]
        public string CustomerCode { get; set; }

        [ExcelColumn(8)]
        public string Category { get; set; }

        [ExcelColumn(9)]
        public string NetWorth { get; set; }

        [ExcelColumn(7)]
        public string OrderDate { get; set; }

        [ExcelColumn(21)]
        public string QuotationStatus { get; set; }
    }
}
