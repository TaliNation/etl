﻿using System;
using ETLBox.DataFlow;

namespace ETLBoxStudy.Dtos
{
    public class OrderTransformedDto
    {
        [ColumnMap("code")]
        public long Code { get; set; }

        [ColumnMap("code_vendeur")]
        public int SalesCode { get; set; }

        [ColumnMap("code_client")]
        public int CustomerCode { get; set; }

        [ColumnMap("categorie")]
        public string Category { get; set; }

        [ColumnMap("valeur_nette")]
        public long NetWorth { get; set; }

        [ColumnMap("date_commande")]
        public DateTime OrderDate { get; set; }

        [ColumnMap("statut_devis")]
        public string QuotationStatus { get; set; }
    }

}
