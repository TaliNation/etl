﻿using ETLBox.DataFlow;

namespace ETLBoxStudy.Dtos
{
    public class SaleTransformedDto
    {
        [ColumnMap("code")]
        public string Code { get; set; }

        [ColumnMap("nom_prenom")]
        public string Name { get; set; }

        [ColumnMap("objectif")]
        public long Goal { get; set; }
    }
}
