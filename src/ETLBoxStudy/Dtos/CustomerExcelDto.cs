﻿using ETLBox.DataFlow;

namespace ETLBoxStudy.Dtos
{
    public class CustomerExcelDto
    {
        [ExcelColumn(2)]
        public string Code { get; set; }

        [ExcelColumn(3)]
        public string Designation { get; set; }

        [ExcelColumn(11)]
        public string Country { get; set; }

        [ExcelColumn(13)]
        public string GroupName { get; set; }

        [ExcelColumn(14)]
        public string SalesCode { get; set; }
    }
}
