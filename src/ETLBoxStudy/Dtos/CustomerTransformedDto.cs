﻿using ETLBox.DataFlow;

namespace ETLBoxStudy.Dtos
{
    public class CustomerTransformedDto
    {
        [ColumnMap("code")]
        public int Code { get; set; }

        [ColumnMap("designation")]
        public string Designation { get; set; }

        [ColumnMap("pays")]
        public string Country { get; set; }

        [ColumnMap("groupe")]
        public string GroupName { get; set; }

        [ColumnMap("code_vendeur")]
        public int? SalesCode { get; set; }
    }
}
