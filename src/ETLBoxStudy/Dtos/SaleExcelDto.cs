﻿using ETLBox.DataFlow;

namespace ETLBoxStudy.Dtos
{
    public class SaleExcelDto
    {
        [ExcelColumn(3)]
        public string Code { get; set; }

        [ExcelColumn(5)]
        public string Name { get; set; }

        [ExcelColumn(6)]
        public long Goal { get; set; }
    }
}
