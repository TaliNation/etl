﻿using System;
using System.Globalization;
using DocumentFormat.OpenXml.Office2016.Excel;

namespace ETLBoxStudy
{
    public static class Helpers
    {
        public static string ToNullIfEmptyString(this string str)
        {
            return str == "" ? null : str;
        }

        public static int? ToNullableInt(this string str)
        {
            return string.IsNullOrEmpty(str) ? null : int.Parse(str);
        }

        static long ParseMoreThousandNetWorth(string str)
        {
            var transformedStr = str.Replace(".", "").Replace(",", "");
            return long.Parse(transformedStr);
        }

        static long ParseLessThousandNetWorth(string str)
        {
            return (long)(double.Parse(str) * 100);
        }

        public static long ParseNetWorth(this string str)
        {
            return str.Contains(',') ? ParseMoreThousandNetWorth(str) : ParseLessThousandNetWorth(str);
        }

        public static string CropQuotationStatus(this string str)
        {
            return string.IsNullOrEmpty(str)
                ? null
                : str
                    .Replace("Devis : ", "")
                    .Replace("Ã©", "é");
        }

        public static string GetCategory(this string str)
        {
            return str switch
            {
                "B" => "Devis",
                _ => "Commande",
            };
        }
    }
}
