using System;
using System.Data.Common;
using System.Globalization;
using ETLBox.Connection;
using ETLBox.DataFlow;
using ETLBox.DataFlow.Connectors;
using ETLBox.DataFlow.Transformations;
using ETLBoxStudy.Dtos;
using Npgsql;

namespace ETLBoxStudy
{
    public class Workflow : IDisposable
    {
        readonly PostgresConnectionManager ConnectionManager;
        readonly NpgsqlConnection Connection;

        public Workflow(string connectionString)
        {
            ConnectionManager = new(connectionString);
            Connection = new(connectionString);
        }

        public void DeleteAllData()
        {
            Connection.Open();
            using (var command = Connection.CreateCommand())
            {
                command.CommandText = "CALL public.delete_all_data()";
                command.ExecuteNonQuery();
            }
            Connection.Close();
        }

        public void LoadSales()
        {
            var source = new ExcelSource<SaleExcelDto>("./data/sales.xlsx");
            var transformation = new RowTransformation<SaleExcelDto, SaleTransformedDto>(
                row => new()
                {
                    Code = row.Code,
                    Name = row.Name,
                    Goal = (row.Goal < 0 ? 0 : row.Goal * 100),
                });
            var destination = new DbDestination<SaleTransformedDto>(ConnectionManager, "vendeurs");

            source.LinkTo(transformation);
            transformation.LinkTo(destination);

            Network.Execute(source);
        }

        public void LoadCustomers()
        {
            // Extract
            var source = new ExcelSource<CustomerExcelDto>("./data/customers.xls");

            // Transform
            var transformation = new RowTransformation<CustomerExcelDto, CustomerTransformedDto>(
                row => new()
                {
                    Code = int.Parse(row.Code),
                    Designation = row.Designation,
                    Country = row.Country,
                    GroupName = row.GroupName.ToNullIfEmptyString(),
                    SalesCode = row.SalesCode == null ? null : int.Parse(row.SalesCode),
                });

            // Load
            var destination = new DbDestination<CustomerTransformedDto>(ConnectionManager, "clients");

            source.LinkTo(transformation);
            transformation.LinkTo(destination);

            Network.Execute(source);
        }

        public void LoadOrders(int rowRangeFrom, int rowRangeTo = 0)
        {
            var source = new ExcelSource<OrderExcelDto>("./data/orders.xls")
            {
                Range = rowRangeTo == 0 ? new(1, rowRangeFrom) : new(1, rowRangeFrom, 22, rowRangeTo),
            };
            var distinct = new Distinct<OrderExcelDto>();

            var transformation = new RowTransformation<OrderExcelDto, OrderTransformedDto>(
            row => new()
            {
                Code = long.Parse(row.Code),
                SalesCode = int.Parse(row.SalesCode),
                CustomerCode = int.Parse(row.CustomerCode),
                Category = row.Category.GetCategory(),
                NetWorth = row.NetWorth.ParseNetWorth(),
                OrderDate =  DateTime.ParseExact(row.OrderDate, "dd.MM.yyyy", CultureInfo.InvariantCulture),
                QuotationStatus = row.QuotationStatus.CropQuotationStatus(),
            });

            var destination = new DbDestination<OrderTransformedDto>(ConnectionManager, "offrescommandes");

            source.LinkTo(distinct);
            distinct.LinkTo(transformation);
            transformation.LinkTo(destination);

            Network.Execute(source);
        }

        public void UpdateSalespeopleCode()
        {
            Connection.Open();
            using (var command = Connection.CreateCommand())
            {
                command.CommandText = "CALL public.jointure_commandes_vendeurs()";
                command.ExecuteNonQuery();
            }
            Connection.Close();
        }

        public void FillAnalyticsTable()
        {
            Connection.Open();
            using (var command = Connection.CreateCommand())
            {
                command.CommandText = "CALL public.fill_analytics_table()";
                command.ExecuteNonQuery();
            }
            Connection.Close();
        }

        public void Dispose()
        {
            ConnectionManager.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
