﻿using System;
using Microsoft.Extensions.Configuration;

namespace ETLBoxStudy.Console
{
    internal static class Program
    {
        static void Main()
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var connectionString = config.GetConnectionString("Bi");

            using var workflow = new Workflow(connectionString);
            workflow.DeleteAllData();
            workflow.LoadSales();
            workflow.LoadCustomers();
            workflow.LoadOrders(1, 5000);
            workflow.LoadOrders(5000, 10000);
            workflow.LoadOrders(10000);
            workflow.UpdateSalespeopleCode();
            workflow.FillAnalyticsTable();
        }
    }
}
